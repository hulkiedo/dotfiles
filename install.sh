#!/bin/bash

oldfiles="$HOME/.oldfiles"
homebin="$HOME/.bin"
nvimpath="$HOME/.config/nvim"

scripts_dir="scripts"
dotfiles_dir="dots"

scripts="packages update ssh-github backup timer cpu memstat"
dotfiles="Xresources bash_aliases bashrc gdbinit gitconfig vimrc"

mkdir -p "$oldfiles"
mkdir -p "$homebin"
mkdir -p "$nvimpath"

help_msg() {
    echo -e "usage: ./install.sh [-a|-i|-I|-p|-h]\n"
    echo -e "\t-a - install scripts and dotfiles"
    echo -e "\t-i - install scripts"
    echo -e "\t-I - install dotfiles"
    echo -e "\t-p - install vimplug"
    echo -e "\t-h - prints this help message"
    exit 0
}

move_oldfiles() {
    for file in $dotfiles; do
        [ -f "$HOME/.$file" ] && mv "$HOME/.$file" "$oldfiles"
    done
    unset file
}

install_vimplug() {
    sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    [ -d "$nvimpath" ] && [ -f "$nvimpath/init.vim" ] && mv "$nvimpath/init.vim" "$oldfiles"
    ln -s "$PWD/$dotfiles_dir/neovim" "$nvimpath/init.vim"
    nvim -c "PlugUpdate" -c "qa"
}

install_dotfiles() {
    move_oldfiles

    for file in $dotfiles; do
        ln -s "$PWD/$dotfiles_dir/$file" "$HOME/.$file"
    done
    unset file

    echo "dotfiles installed to $HOME, others dotfiles moved to $oldfiles"
}

install_scripts() {
    for file in $scripts; do
        cp "$scripts_dir/$file" "$homebin/$file"
        chmod +x "$homebin/$file"
    done
    unset file

    echo "scripts installed to $homebin"
}

if [ $# -lt 1 ]; then
    help_msg
fi

while getopts ":aiIph" opt; do
    case $opt in
        a)
            install_scripts
            install_dotfiles
            ;;
        i) install_scripts ;;
        I) install_dotfiles ;;
        p) install_vimplug ;;
        h) help_msg ;;
        *) echo "unknown option" ;;
    esac
done

exit 0
