# Dotfiles

My collection of dotfiles and scripts

# How to use

firstly, you clone these repository to your work directory, then go to repository folder, and run this command:
```sh
$ ./install.sh -a
```

Options:
- -a - install scripts, dotfiles and vim-plug
- -i - install scripts
- -I - install dotfiles
- -p - install vim-plug
- -h - prints this help message

# How it works

install.sh makes symbolic links for dotfiles in $HOME directory, and also creates symbolic link for neovim config file. Also this script copies scripts to ~/.bin directory and makes it's executable. if there are dotfiles in the home directory that conflict with files in this repository, they are moved to the ~/.oldfiles directory

# TODO

- [x] Add check for existing dotfiles
- [ ] Add tmux.conf

# License

MIT License
